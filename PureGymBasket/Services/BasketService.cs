﻿using PureGymBasket.Data;
using PureGymBasket.Data.Entities;
using PureGymBasket.Enums;
using PureGymBasket.Helpers;
using PureGymBasket.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PureGymBasket.Services
{
    /// <summary>
    /// This is a BasketService
    /// </summary>
    public class BasketService:IBasketService
    {
        private readonly IDbContext _dbContext;
        public BasketService(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Apply Voucher
        /// </summary>
        /// <param name="voucher"></param>
        /// <param name="basketId"></param>
        /// <returns> Result </returns>
        public Result ApplyVoucher(List<Voucher> vouchers, int basketId)
        {
            var result = new Result();

            var basket = _dbContext.Baskets.FirstOrDefault(x => x.Id == basketId);

            if (basket != null)
            {
                foreach(var voucher in vouchers)
                {
                    switch (voucher.Type)
                    {
                        case (int)VoucherType.Offer:
                            if (basket.TotalPriceForItems < voucher.MinimumEligibleBasketAmount)
                            {
                                result.Success = false;
                                result.Message = "Minimum amount not reached";
                            }
                            else
                            {
                                if (basket.TotalEligibleProductAmount > 0)
                                {
                                    var voucherApplied = new Voucher
                                    {
                                        Id = voucher.Id,
                                        Code = voucher.Code,
                                    };

                                    if (basket.TotalEligibleProductAmount > voucher.Amount)
                                        voucherApplied.Amount = voucher.Amount;
                                    voucherApplied.Amount = basket.TotalEligibleProductAmount;

                                    basket.Vouchers.Add(voucherApplied);
                                    _dbContext.SaveChanges();

                                    result.Total = basket.TotalAfterVouchers;
                                    result.Success = true;
                                    result.Message = "Applied";
                                }
                                else
                                {
                                    result.Success = false;
                                    result.Message = $"There are no products in your basket applicable to voucher {voucher.Code}";
                                    result.Total = basket.TotalAfterVouchers;
                                }
                            }
                            break;
                        case (int)VoucherType.Gift:
                            basket.Vouchers.Add(voucher);
                            _dbContext.SaveChanges();
                            result.Total = basket.TotalAfterVouchers;
                            result.Success = true;
                            result.Message = "Applied";
                            break;
                        default:
                            break;
                    }
                }
            }

            return result;
        }

    }
}
