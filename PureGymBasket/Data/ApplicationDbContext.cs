﻿using Microsoft.EntityFrameworkCore;
using PureGymBasket.Data.Entities;

namespace PureGymBasket.Data
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
    }
}
