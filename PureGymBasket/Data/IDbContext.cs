﻿using Microsoft.EntityFrameworkCore;
using PureGymBasket.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PureGymBasket.Data
{
    public interface IDbContext
    {
        DbSet<Basket> Baskets { get; set; }
        int SaveChanges();
    }
}
