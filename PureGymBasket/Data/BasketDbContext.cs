﻿using Microsoft.EntityFrameworkCore;
using PureGymBasket.Data.Entities;

namespace PureGymBasket.Data
{
    public class BasketDbContext : DbContext
    {
        public BasketDbContext(DbContextOptions<BasketDbContext> options)
            : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<BasketItem> BasketItems { get; set; }
        public DbSet<Basket> Basket { get; set; }

    }
}
