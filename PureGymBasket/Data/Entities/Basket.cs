﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PureGymBasket.Data.Entities
{
    /// <summary>
    /// Basket Entity
    /// </summary>
    public class Basket
    {
        public Basket()
        {
            this.Vouchers = new List<Voucher>();
        }
        public int Id { get; set; }
        public List<BasketItem> BasketItems { get; set; }
        public List<Voucher> Vouchers { get; set; }
        public double TotalPriceForItems
        {
            get
            {
                return this.EnsureValueIsNotNegative(this.BasketItems.Sum(x => x.SubTotal));
            }
        }

        public double VouchersReedemedAmount { get; set; }

        public double TotalAfterVouchers
        {
            get
            {
                return Math.Round(this.EnsureValueIsNotNegative(this.BasketItems.Sum(x => x.SubTotal) - Vouchers.Sum(x => x.Amount)), 2);
            }
        }

        public double TotalEligibleProductAmount
        {
            get
            {
                return this.EnsureValueIsNotNegative(this.BasketItems.Where(x => x.Product.CanApplyOfferVoucher).Sum(x => x.SubTotal));
            }
        }
        private double EnsureValueIsNotNegative(double value)
        {
            return value < 0 ? 0D : value;
        }

    }
}
