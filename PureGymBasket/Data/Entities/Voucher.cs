﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PureGymBasket.Data.Entities
{
    public class Voucher
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int Type { get; set; }
        public double Amount { get; set; }
        public double MinimumEligibleBasketAmount { get; set; } = 50;
    }
}
