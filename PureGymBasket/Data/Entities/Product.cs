﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PureGymBasket.Data.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public bool CanApplyOfferVoucher { get; set; }
    }
}
