﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PureGymBasket.Data.Entities
{
    public class BasketItem
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public double SubTotal
        {
            get
            {
                return this.EnsureValueIsNotNegative(Product.Price * this.Quantity);
            }
        }
        private double EnsureValueIsNotNegative(double value)
        {
            return value < 0 ? 0D : value;
        }
    }
}
