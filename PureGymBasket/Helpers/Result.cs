﻿namespace PureGymBasket.Helpers
{
    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public double Total { get; set; }
    }
}
