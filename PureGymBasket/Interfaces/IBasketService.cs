﻿using PureGymBasket.Data.Entities;
using PureGymBasket.Helpers;
using System.Collections.Generic;

namespace PureGymBasket.Interfaces
{
    /// <summary>
    /// BasketService Interface
    /// </summary>
    public interface IBasketService
    {
        Result ApplyVoucher(List<Voucher> vouchers, int basketId);
    }
}
