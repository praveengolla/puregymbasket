using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using PureGymBasket.Data;
using PureGymBasket.Data.Entities;
using PureGymBasket.Services;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    [TestFixture]
    public class BasketServiceTest
    {
        BasketService basketService;
        Mock<IDbContext> mockDataStore;

        [SetUp]
        public void Setup()
        {
            mockDataStore = new Mock<IDbContext>();

            basketService = new BasketService(mockDataStore.Object);
        }

        [Test]
        public void WhenBasketLessThan50_ApplyOfferVoucher_ReturnsFalse()
        {
            var vouchers = new List<Voucher>()
            {
                new Voucher
                {
                    Amount = 5,
                    Code = "YYY-YYY",
                    Type = 2,
                    Id = 1
                }
            };
            var basket = new List<Basket>
            {
               new Basket{ Id = 1, BasketItems = LoadBasketItems() }
            };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var res = basketService.ApplyVoucher(vouchers, 1);

            Assert.IsFalse(res.Success);
        }

        [Test]
        public void WhenBasket1_ApplyGiftVoucher_Returns60_15()
        {
            var giftVouchers = new List<Voucher> { GetGiftVoucherBasket1() };
            var basket1 = GetBasket1();
            var basket = new List<Basket> { basket1 };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var res = basketService.ApplyVoucher(giftVouchers, 1);

            Assert.AreEqual(60.15, res.Total);
        }

        [Test]
        public void WhenBasket2_ApplyOfferVoucher_Returns51()
        {
            var offerVouchers =new List<Voucher> { GetOfferVoucherBasket2() };
            var basket2 = GetBasket2();
            var basket = new List<Basket> { basket2 };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var result = basketService.ApplyVoucher(offerVouchers, 2);

            Assert.AreEqual(51,result.Total);
        }

        [Test]
        public void WhenBasket3_ApplyOfferVoucher_Returns51()
        {
            var offerVoucher = new List<Voucher> { GetOfferVoucherBasket3() };
            var basket3 = GetBasket3();
            var basket = new List<Basket> { basket3 };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var result = basketService.ApplyVoucher(offerVoucher, 3);

            Assert.AreEqual(51,result.Total);

        }

        [Test]
        public void WhenBasket4_ApplyTwoVouchers_Returns41()
        {
            var offerVoucher = new List<Voucher> { GetOfferVoucherBasket4(), GetGiftVoucherBasket4() };
            var basket4 = GetBasket4();
            var basket = new List<Basket> { basket4 };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var result = basketService.ApplyVoucher(offerVoucher, 4);

            Assert.AreEqual(41, result.Total);
        }



        [Test]
        public void WhenBasket5_ApplyTwoVouchers_Returns55()
        {
            var offerVoucher = new List<Voucher> { GetOfferVoucherBasket5() };
            var basket5 = GetBasket5();
            var basket = new List<Basket> { basket5 };
            var basketTestData = MockDbSet(basket);
            mockDataStore.Setup(x => x.Baskets).Returns(basketTestData.Object);

            var result = basketService.ApplyVoucher(offerVoucher, 5);

            Assert.AreEqual(55, result.Total);

        }


        private Voucher LoadOfferVoucher()
        {
            return new Voucher()
            {
                Id = 2,
                Amount = 5,
                Code = "YYY-YYY",
                Type = 2
            };
        }

        #region Basket1 Test Data
        private Basket GetBasket1()
        {
            var basket = new Basket()
            {
                Id = 1,
                BasketItems = new List<BasketItem>()
                {
                    new BasketItem()
                    {
                        Id =1,
                        Quantity =1,
                        Product = new Product()
                        {
                            Id=1,
                            Name="Hat",
                            Price=10.50
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=2,
                            Name="Jumper",
                            Price=54.65
                        }
                    }
                }
            };

            return basket;
        }

        private Voucher GetGiftVoucherBasket1()
        {
            return new Voucher()
            {
                Id = 1,
                Amount = 5.00,
                Code = "XXX-XXX",
                Type = 1
            };
        }

        #endregion

        #region Basket2 Test Data
        private Basket GetBasket2()
        {
            var basket = new Basket()
            {
                Id = 2,
                BasketItems = new List<BasketItem>()
                {
                    new BasketItem()
                    {
                        Id =1,
                        Quantity =1,
                        Product = new Product()
                        {
                            Id=1,
                            Name="Hat",
                            Price=25.00,
                            CanApplyOfferVoucher=false
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=2,
                            Name="Jumper",
                            Price=26.00,
                            CanApplyOfferVoucher=false
                        }
                    }
                }
            };

            return basket;
        }

        private Voucher GetOfferVoucherBasket2()
        {
            return new Voucher()
            {
                Id = 1,
                Amount = 5,
                Code = "YYY-YYY",
                Type =2
            };
        }
        #endregion

        #region Basket3 Test Data

        private Basket GetBasket3()
        {
            var basket = new Basket()
            {
                Id = 3,
                BasketItems = new List<BasketItem>()
                {
                    new BasketItem()
                    {
                        Id =1,
                        Quantity =1,
                        Product = new Product()
                        {
                            Id=1,
                            Name="Hat",
                            Price=25.00,
                            CanApplyOfferVoucher=false
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=2,
                            Name="Jumper",
                            Price=26.00,
                            CanApplyOfferVoucher=false
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=3,
                            Name="Head Light",
                            Price=3.50,
                            CanApplyOfferVoucher=true
                        }
                    }
                }
            };

            return basket;
        }

        private Voucher GetOfferVoucherBasket3()
        {
            return new Voucher()
            {
                Id = 1,
                Amount = 5,
                Code = "YYY-YYY",
                Type = 2
            };
        }
        #endregion

        #region Basket4 Test Data
        private Basket GetBasket4()
        {
            var basket = new Basket()
            {
                Id = 4,
                BasketItems = new List<BasketItem>()
                {
                    new BasketItem()
                    {
                        Id =1,
                        Quantity =1,
                        Product = new Product()
                        {
                            Id=1,
                            Name="Hat",
                            Price=25.00,
                            CanApplyOfferVoucher=true
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=2,
                            Name="Gift Voucher",
                            Price=26.00,
                            CanApplyOfferVoucher=false
                        }
                    }
                }
            };

            return basket;
        }

        private Voucher GetGiftVoucherBasket4()
        {
            return new Voucher()
            {
                Id = 1,
                Amount = 5,
                Code = "XXX-XXX",
                Type = 1
            };
        }

        private Voucher GetOfferVoucherBasket4()
        {
            return new Voucher()
            {
                Id = 2,
                Amount = 5,
                Code = "YYY-YYY",
                Type = 2
            };
        }

        #endregion

        #region Basket5 Test Data
        private Basket GetBasket5()
        {
            var basket = new Basket()
            {
                Id = 5,
                BasketItems = new List<BasketItem>()
                {
                    new BasketItem()
                    {
                        Id =1,
                        Quantity =1,
                        Product = new Product()
                        {
                            Id=1,
                            Name="Hat",
                            Price=25.00,
                            CanApplyOfferVoucher=true
                        }
                    },
                    new BasketItem()
                    {
                        Id=2,
                        Quantity=1,
                        Product=new Product()
                        {
                            Id=2,
                            Name="Gift Voucher",
                            Price=30.00,
                            CanApplyOfferVoucher=false
                        }
                    }
                }
            };

            return basket;
        }

        private Voucher GetOfferVoucherBasket5()
        {
            return new Voucher()
            {
                Id = 1,
                Amount = 5,
                Code = "YYY-YYY",
                Type = 2
            };
        }

        #endregion

        Mock<DbSet<T>> MockDbSet<T>(IEnumerable<T> list) where T : class, new()
        {
            IQueryable<T> queryableList = list.AsQueryable();
            Mock<DbSet<T>> dbSetMock = new Mock<DbSet<T>>();
            dbSetMock.As<IQueryable<T>>().Setup(x => x.Provider).Returns(queryableList.Provider);
            dbSetMock.As<IQueryable<T>>().Setup(x => x.Expression).Returns(queryableList.Expression);
            dbSetMock.As<IQueryable<T>>().Setup(x => x.ElementType).Returns(queryableList.ElementType);
            dbSetMock.As<IQueryable<T>>().Setup(x => x.GetEnumerator()).Returns(() => queryableList.GetEnumerator());
            //dbSetMock.Setup(x => x.Create()).Returns(new T());

            return dbSetMock;
        }

        private List<BasketItem> LoadBasketItems()
        {
            List<BasketItem> basketItems = new List<BasketItem>()
            {
                new BasketItem() { Id=1, Quantity=1, Product= new Product{ Name="Hat", Id=1, Price=30, CanApplyOfferVoucher=true } }
            };

            return basketItems;
        }
    }
}